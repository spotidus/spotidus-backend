package da.gammla

import java.security.MessageDigest
import java.security.SecureRandom

object Utils {

    val random = SecureRandom()

    fun generateToken(): String {
        return String(CharArray(Globals.TOKEN_LENGTH) {
            Globals.TOKEN_CHARS[random.nextInt(Globals.TOKEN_CHARS.length)]
        })
    }

    fun generateSalt(): ByteArray {
        val salt = ByteArray(32)
        random.nextBytes(salt)
        return salt
    }

    fun hash(input: ByteArray): ByteArray {
        val digest = MessageDigest.getInstance(Globals.HASH_FUNCTION)
        return digest.digest(input)
    }

    fun checkPassword(provided: String, hashed: ByteArray, salt: ByteArray): Boolean {
        val pwBytes = provided.encodeToByteArray()
        val toCheck = hash(pwBytes + salt)
        return toCheck.contentEquals(hashed)
    }
}