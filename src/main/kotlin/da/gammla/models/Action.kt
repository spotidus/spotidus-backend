package da.gammla.models

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

class Action(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<Action>(Actions)

    enum class Type {
        SONG,
        SKIP
    }

    var timestamp by Actions.timestamp
    var type by Actions.type
    var song by Actions.song
    var user by Actions.user
    var establishment by Actions.establishment
}

object Actions : IntIdTable() {
    val timestamp = long("timestamp").index()
    val type = enumeration<Action.Type>("type")
    val song = varchar("song", 32)

    val user = reference("user", Users).index()
    val establishment = reference("establishment", Establishments).index()
}