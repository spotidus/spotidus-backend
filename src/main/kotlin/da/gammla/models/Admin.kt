package da.gammla.models

import da.gammla.Globals
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

class Admin(id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Admin>(Admins)

    var login by Admins.login
    var passwordHash by Admins.passwordHash
    var passwordSalt by Admins.passwordSalt

    var token by Admins.token
}

object Admins : IntIdTable() {
    val login = varchar("login", length = Globals.NAME_LENGTH).index()

    val passwordHash = binary("password_hash", length = 32)
    val passwordSalt = binary("password_salt", length = 32)

    val token = varchar("token", length = Globals.TOKEN_LENGTH).index()
}
