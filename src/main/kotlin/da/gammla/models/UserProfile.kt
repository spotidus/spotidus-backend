package da.gammla.models

import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

class UserProfile(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<UserProfile>(UserProfiles)

    var establishment by UserProfiles.establishment
    var actionTimeframe by UserProfiles.actionTimeframe
    var skips by UserProfiles.skips
    var songs by UserProfiles.songs
    var canPlayPause by UserProfiles.canPlayPause
    var canPickName by UserProfiles.canPickName
}

object UserProfiles : IntIdTable() {
    val establishment = reference("establishment", Establishments).index()
    val actionTimeframe = long("action_timeframe")
    val skips = long("skips")
    val songs = long("song")
    val canPlayPause = bool("can_play_pause")
    val canPickName = bool("can_pick_name")
}