package da.gammla.models

import da.gammla.Globals
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

class Establishment (id: EntityID<Int>) : IntEntity(id) {
    companion object : IntEntityClass<Establishment>(Establishments)


    var login by Establishments.login
    var name by Establishments.name
    var spotifyAccessToken by Establishments.spotifyAccessToken
    var spotifyRefreshToken by Establishments.spotifyRefreshToken
    var deleteExpired by Establishments.deleteExpired
    var token by Establishments.token
    var passwordHash by Establishments.passwordHash
    var passwordSalt by Establishments.passwordSalt
}

object Establishments : IntIdTable() {
    val login = varchar("login", length = Globals.NAME_LENGTH).default("").index()
    val name = varchar("name", length = Globals.NAME_LENGTH).default("")
    val spotifyAccessToken = varchar("spotify_access", length = 256).default("")
    val spotifyRefreshToken = varchar("spotify_refresh", length = 256).default("")
    val deleteExpired = bool("delete_expired").default(false)


    val token = varchar("token", length = Globals.TOKEN_LENGTH).index()
    val passwordHash = binary("password_hash", length = 32).default(ByteArray(32))
    val passwordSalt = binary("password_salt", length = 32).default(ByteArray(32))
}
