package da.gammla.models

import da.gammla.Globals
import org.jetbrains.exposed.dao.IntEntity
import org.jetbrains.exposed.dao.IntEntityClass
import org.jetbrains.exposed.dao.id.EntityID
import org.jetbrains.exposed.dao.id.IntIdTable

class User(id: EntityID<Int>) : IntEntity(id) {

    companion object : IntEntityClass<User>(Users)

    var token by Users.token
    var name by Users.name
    var establishment by Users.establishment
    var expiryTime by Users.expiryTime
    var actionTimeframe by Users.actionTimeframe
    var skips by Users.skips
    var songs by Users.songs
    var canPlayPause by Users.canPlayPause
    var canPickName by Users.canPickName
}

object Users : IntIdTable() {
    val token = varchar("token", length = Globals.TOKEN_LENGTH).index()
    val name = varchar("name", length = Globals.NAME_LENGTH)
    val establishment = reference("establishment", Establishments)
    val expiryTime = long("expiry_time")

    val actionTimeframe = long("action_timeframe")
    val skips = long("skips")
    val songs = long("song")
    val canPlayPause = bool("can_play_pause")
    val canPickName = bool("can_pick_name")
}