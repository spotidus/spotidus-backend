package da.gammla

import da.gammla.services.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.engine.*
import io.ktor.server.netty.*
import io.ktor.server.plugins.contentnegotiation.*
import io.ktor.server.plugins.cors.routing.*
import java.security.KeyStore

fun main() {
    embeddedServer(Netty, environment = environment, configure = serverConfig)
        .start(wait = true)
}

val serverConfig: ApplicationEngine.Configuration.() -> Unit = {
    /*connectionGroupSize = 1
    workerGroupSize = 2
    callGroupSize = 1*/
}

val environment = applicationEngineEnvironment {
    log = Globals.logger
    if (Globals.httpPort >= 0) {
        connector {
            host = Globals.host
            port = Globals.httpPort
        }
    }

    if (Globals.httpsPort >= 0 && Globals.keyStoreFile.exists()) {

        sslConnector(
            keyStore = KeyStore.getInstance(Globals.keyStoreFile, Globals.keyStorePassword.toCharArray(0)),
            keyAlias = Globals.sslPrivateKeyAlias,
            keyStorePassword = { Globals.keyStorePassword.toCharArray(0) },
            privateKeyPassword = { Globals.sslPrivateKeyPassword.toCharArray(0) }) {
            host = Globals.host
            port = Globals.httpsPort
            keyStorePath = Globals.keyStoreFile
        }
    }

    this.module {
        configureModules()
    }
}

fun Application.configureModules() {

    //Has to be first
    DatabaseService.configure()

    EstablishmentService.configure(this)
    UserService.configure(this)
    AdminService.configure(this)

    install(ContentNegotiation) {
        json(contentType = ContentType.Any)
    }

    install(CORS) {
        Globals.allowedCorsHosts.split(",").forEach { host ->
            val split = host.trim().split("://")
            if (split.size == 2){
                allowHost(split[1], schemes = listOf(split[0]))
            } else {
                allowHost(split[0])
            }
        }
        allowCredentials = true
    }
}
