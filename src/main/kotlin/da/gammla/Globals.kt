package da.gammla

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.io.File
import java.io.FileInputStream
import java.nio.file.Paths
import java.util.*


private fun loadProps() = Properties().apply {

    val workingDir = System.getProperty("user.dir")

    val configFile = Paths.get(workingDir, "app.config").toFile()

    Globals.logger.info("Working dir: $workingDir")

    Globals.logger.info("Reading config file")
    try {
        load(FileInputStream(configFile))
    } catch (_:Exception) {
        Globals.logger.error("Config file not found! It should be located at ${configFile.absolutePath}")
    }
}

object Globals {
    val logger: Logger = LoggerFactory.getLogger("Cafe Play")

    private val props = loadProps()

    val host = props.getProperty("host") ?: "0.0.0.0"

    val httpPort = Integer.parseInt(props.getProperty("httpPort") ?: "8080")
    val httpsPort = Integer.parseInt(props.getProperty("httpsPort") ?: "8443")

    val keyStoreFile = File(props.getProperty("keyStore") ?: "")
    val keyStorePassword = props.getProperty("keyStorePassword") ?: ""
    val sslPrivateKeyAlias = props.getProperty("sslPrivateKeyAlias") ?: ""
    val sslPrivateKeyPassword = props.getProperty("sslPrivateKeyPassword") ?: ""

    val dbUser = props.getProperty("dbUser") ?: "postgres"
    val dbPassword = props.getProperty("dbPassword") ?: "postgres"
    val dbUrl = props.getProperty("dbUrl") ?: "postgresql://localhost:5432/cafe-play"

    val allowedCorsHosts = props.getProperty("allowedCorsHosts") ?: "*"

    val initialAdminPassword = props.getProperty("initialAdminPassword") ?: "adminpw"
    val initialAdminLogin = props.getProperty("initialAdminLogin") ?: "admin"

    val spotifyClientId = props.getProperty("spotifyClientId") ?: ""
    val spotifyClientSecret = props.getProperty("spotifyClientSecret") ?: ""

    const val SPOTIFY_SCOPES = "user-read-playback-state user-modify-playback-state user-read-currently-playing user-top-read"

    const val TOKEN_LENGTH = 64
    const val NAME_LENGTH = 50

    const val TOKEN_CHARS = "0123456789abcdefghijklmopqrstuvxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"

    const val HASH_FUNCTION = "SHA-256"

}