package da.gammla.services

import da.gammla.Globals
import da.gammla.models.*
import org.jetbrains.exposed.sql.transactions.transaction
import org.jetbrains.exposed.sql.*

object DatabaseService {
    private lateinit var db: Database
    fun <T> transaction(statement: Transaction.() -> T): T = transaction(db, statement)

    fun configure(){
        db = Database.connect(
            url = "jdbc:${Globals.dbUrl}",
            user = Globals.dbUser,
            driver = "org.postgresql.Driver",
            password = Globals.dbPassword
        )

        transaction {
            SchemaUtils.create(
                Admins,
                Users,
                Establishments,
                Actions,
                UserProfiles
            )
        }
    }
}

typealias DB = DatabaseService