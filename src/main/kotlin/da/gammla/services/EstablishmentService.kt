package da.gammla.services

import da.gammla.Globals
import da.gammla.Utils
import da.gammla.models.Establishment
import da.gammla.models.Establishments
import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.engine.cio.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.client.request.*
import io.ktor.client.request.forms.*
import io.ktor.http.*
import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.jetbrains.exposed.sql.transactions.transaction

object EstablishmentService {
    fun configure(app: Application){
        app.routing {
            initialToken()
            initialize()
            readiness()
            oauthConfig()
            oauthCode()
        }
    }

    private val httpClient = HttpClient(CIO) {
        install(ContentNegotiation) {
            json(Json {
                ignoreUnknownKeys = true
            })
        }
    }

    private fun Routing.initialToken() = post("/establishment/initial_token") {

        val token = call.receiveText()

        val estab = transaction {

            Establishment.find {
                Establishments.token eq token
            }.firstOrNull()
        }

        if (estab == null){
            badRequest("Invalid token")
            return@post
        }

        val newToken = Utils.generateToken()

        transaction {
            estab.token = newToken
        }

        respondToken(newToken)
    }

    private fun Routing.initialize() = post("/establishment/initialize") {

        val estab = getEstablishmentByToken()

        if (estab == null){
            unauthorized("Invalid token")
            return@post
        }

        val init = call.receive<EstablishmentInitialize>()

        val newToken = Utils.generateToken()
        val newSalt = Utils.generateSalt()
        val pwHash = Utils.hash(init.password.encodeToByteArray() + newSalt)

        transaction {
            estab.apply {
                login = init.login
                name = init.name
                passwordHash = pwHash
                passwordSalt = newSalt
                token = newToken
            }
        }

        respondToken(newToken)
    }
    @Serializable
    private data class EstablishmentInitialize(val login: String, val name: String, val password: String)

    private fun Routing.readiness() = get("/establishment/readiness") {

        val estab = getEstablishmentByToken()

        if (estab == null){
            unauthorized("invalid")
        } else if (estab.login == "") {
            call.respond("init")
        } else if (estab.spotifyRefreshToken == ""){
            call.respond("oauth")
        } else {
            call.respond("ready")
        }
    }

    private fun Routing.oauthConfig() = get("/establishment/oauth_config") {

        val estab = getEstablishmentByToken()

        if (estab == null){
            unauthorized("Invalid Token")
            return@get
        }

        call.respond(SpotifyOauthConfig(Globals.spotifyClientId, Globals.SPOTIFY_SCOPES))
    }
    @Serializable
    private data class SpotifyOauthConfig(val clientId: String, val scope: String)

    private fun Routing.oauthCode() = post("/establishment/oauth_code") {

        val estab = getEstablishmentByToken()

        if (estab == null){
            unauthorized("Invalid Token")
            return@post
        }

        val authCode = call.receive<SpotifyOauthCode>()


        val response = httpClient.submitForm(
            url = "https://accounts.spotify.com/api/token",
            formParameters = parameters {
                append("code", authCode.code)
                append("redirect_uri", authCode.redirectUri)
                append("grant_type", "authorization_code")
            }, block = { basicAuth(Globals.spotifyClientId, Globals.spotifyClientSecret) }
        )

        val spotifyTokens = response.body<SpotifyOauthTokens>()

        transaction {
            estab.apply {
                spotifyAccessToken = spotifyTokens.accessToken
                spotifyRefreshToken = spotifyTokens.refreshToken
            }
        }

        call.respondText("Success")
    }
    @Serializable
    private data class SpotifyOauthCode(val code: String, val redirectUri: String)
    @Serializable
    private data class SpotifyOauthTokens(
        @SerialName("access_token") val accessToken: String,
        @SerialName("refresh_token") val refreshToken: String
    )

    private suspend fun Req.unauthorized(msg: String = ""){
        call.respondText(msg, status = HttpStatusCode.Unauthorized)
    }

    private suspend fun Req.badRequest(msg: String = ""){
        call.respondText(msg, status = HttpStatusCode.BadRequest)
    }

    private suspend fun Req.respondToken(token: String){
        call.response.cookies.append("establishment_token", token, maxAge = Long.MAX_VALUE)
        call.respondText(token)
    }

    private fun Req.getEstablishmentByToken(): Establishment? {
        val providedToken = call.request.cookies["establishment_token"] ?: return null

        return transaction {
            val query = Establishment.find {
                Establishments.token eq providedToken
            }

            query.firstOrNull()
        }

    }
}


