package da.gammla.services

import da.gammla.Globals
import da.gammla.Utils
import da.gammla.models.Admin
import da.gammla.models.Admins
import da.gammla.models.Establishment
import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import io.ktor.util.pipeline.*
import kotlinx.serialization.Serializable
import org.jetbrains.exposed.sql.transactions.transaction

typealias Req = PipelineContext<Unit, ApplicationCall>

object AdminService {
    fun configure(app: Application){
        createInitialAdmin()


        app.routing {
            login()
            pwchange()
            createEstablishment()
        }
    }

    private fun createInitialAdmin(){
        transaction {
            if (Admin.count() == 0L) {

                val salt = Utils.generateSalt()

                Admin.new {
                    login = Globals.initialAdminLogin
                    passwordSalt = salt
                    passwordHash = Utils.hash(
                        Globals.initialAdminPassword.encodeToByteArray() + salt
                    )
                    token = Utils.generateToken()
                }
            }
        }
    }

    private fun Routing.login() = post("/admin/login") {
        val (login, password, newToken) = call.receive<AdminLogin>()

        val admin = transaction {

            Admin.find {
                Admins.login eq login
            }.firstOrNull()

        }

        if (admin == null){
            unauthorized("Wrong password")
            return@post
        }

        val passwordCorrect = Utils.checkPassword(password, admin.passwordHash, admin.passwordSalt)

        if (!passwordCorrect){
            unauthorized("Wrong password")
            return@post
        }

        val token: String
        if (newToken == true){
            token = Utils.generateToken()
            transaction {
                admin.token = token
            }
        } else {
            token = admin.token
        }

        respondToken(token)
    }

    @Serializable
    private data class AdminLogin(val login: String, val password: String, val newToken: Boolean? = null)

    private fun Routing.pwchange() = post("/admin/pwchange") {

        val admin = getAdminByToken()

        if (admin == null){
            unauthorized("Invalid admin_token")
            return@post
        }

        val newPw = call.receiveText()
        if (newPw.length >= 6){

            val newToken = Utils.generateToken()
            val newSalt = Utils.generateSalt()

            val pwHash = Utils.hash(newPw.encodeToByteArray() + newSalt)

            transaction {
                admin.token = newToken
                admin.passwordSalt = newSalt
                admin.passwordHash = pwHash
            }

            respondToken(newToken)

        } else {
            badRequest("Password too short")
        }

    }


    private fun Routing.createEstablishment() = post("/admin/create_establishment") {

        val admin = getAdminByToken()

        if (admin == null){
            unauthorized("Invalid admin_token")
            return@post
        }

        //Initial token is short in case it has to be entered by hand
        val newToken = Utils.generateToken().substring(0, 12)

        transaction {
            Establishment.new {
                token = newToken
            }
        }

        call.respondText(newToken)
    }


    private suspend fun Req.unauthorized(msg: String = ""){
        call.respondText(msg, status = HttpStatusCode.Unauthorized)
    }

    private suspend fun Req.badRequest(msg: String = ""){
        call.respondText(msg, status = HttpStatusCode.BadRequest)
    }

    private suspend fun Req.respondToken(token: String){
        //Token is stored in the client for a week
        call.response.cookies.append("admin_token", token, maxAge = 60L * 24 * 7)
        call.respondText(token)
    }

    private fun Req.getAdminByToken(): Admin? {
        val providedToken = call.request.cookies["admin_token"] ?: return null

        return transaction {
            val adminQuery = Admin.find {
                Admins.token eq providedToken
            }

            adminQuery.firstOrNull()
        }

    }
}