package da.gammla.services

import io.ktor.server.application.*
import io.ktor.server.response.*
import io.ktor.server.routing.*

object UserService {
    fun configure(app: Application){
        app.routing {
            get("/user") {
                call.respondText("Hello World!")
            }
        }
    }
}
